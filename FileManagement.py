import hashlib
import json

class FileManagement:
    '''Manage nd5 class'''
    def __init__(self):
        self.path = ''
        self.md5 = ''
        self.fileName = ''

    def setPath(self, path):
        '''Keep path of current file function'''
        self.path = path
        self.md5 = hashlib.md5(open(path,'rb').read()).hexdigest()
        self.fileName = path.split("/")[-1].split(".")[0]

    def getPath(self):
        '''Return current file path function'''
        return self.path

    def getMD5(self):
        '''Return md5 of current file'''
        return self.md5

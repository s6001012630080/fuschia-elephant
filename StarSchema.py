import uuid

import pandas as pd

from datetime import datetime

import json


class StarSchema:

    def __init__(self):
        self.workbook = None
        self.worksheet = None
        self.currentSheet = None

        self.category = {
            'dimensions': [],
            'dimensionsDate': [], 
            'measurements': []
            }
        self._dimensions = {'Date': []}
        self.factTable = None
        self.factTablePD = None

    def loadExcel(self, path):
        self.workbook = pd.ExcelFile(path)
        self.worksheet = {sheetName: self.workbook.parse(sheetName)
                          for sheetName in self.workbook.sheet_names}

    def loadFile(self, path, state):
        if state == 'open':
            path = 'caches/{}'.format(path)
        print('load file', path)
        with open(path, encoding='utf-8') as f:
            data = json.loads(f.read())
            self.currentSheet = data['currentSheet']

            self.category = {
                'dimensions': data['category']['dimensions'],
                'dimensionsDate': data['category']['dimensionsDate'],
                'measurements': data['category']['measurements']
            }
            self._dimensions = data['dimensions']
            self.factTable = data['factTable']

            for i in self.category['dimensionsDate']:
                self.factTable[i+' Timestamp'] = {}
                for j in range(len(self.factTable[list(self.factTable.keys())[0]])):
                    d = datetime.strptime(self.factTable[i][str(j)], '%Y-%m-%d')
                    self.factTable[i+' Timestamp'][str(j)] = d

            self.factTablePD = pd.DataFrame(self.factTable)

    def readWorkSheet(self, sheetName):
        self.currentSheet = sheetName
        self._createStarSchema()

    def _createStarSchema(self):
        sheet = self.worksheet[self.currentSheet]
        """Classify columns"""
        for col, dataType in zip(sheet.columns, sheet.dtypes):
            if dataType == 'int64' or dataType == 'float64': # case number
                self.category['measurements'].append(col)
            elif dataType == 'datetime64[ns]': # case date
                self.category['dimensionsDate'].append(col)
            else: # case other
                self.category['dimensions'].append(col)

        sheet_dict = sheet.to_dict()

        """Create normal dimension"""
        for col in self.category['dimensions']:
            count = -1
            unique = []
            self._dimensions[col] = {}
            for attr in sheet_dict[col]:
                if sheet_dict[col][attr] not in unique:
                    unique.append(sheet_dict[col][attr])
                    count += 1
                    self._dimensions[col][count] = sheet_dict[col][attr]

        """Change the normal data to Fact table structure"""
        ft = []
        keys = {}
        for index in range(len(sheet_dict[list(sheet_dict.keys())[0]])):
            obj = {
                key: self.findKeyByValue(key, sheet_dict[key][index]) if key in self.category['dimensions'] else 
                           datetime.utcfromtimestamp(sheet_dict[key][index].timestamp()).strftime('%Y-%m-%d')
                       if key in self.category['dimensionsDate'] else sheet_dict[key][index]
                   for key in sheet_dict
                   }
            
            for i in self.category['dimensionsDate']:
                string = i+' Timestamp'
                obj[string] = sheet_dict[i][index]
            ft.append(obj)
        self.factTable = ft
        self.factTablePD = pd.DataFrame(ft)

    def findKeyByValue(self, col, val):
        '''Find key by value function'''
        for k,v in self._dimensions[col].items():
            if v == val:
                return k

    def resetStarschema(self):
        '''Reset Starschemar function'''
        self.workbook = None
        self.worksheet = None
        self.currentSheet = None

        self.category = {
            'dimensions': [],
            'dimensionsDate': [],
            'measurements': []
            }
        self._dimensions = {'Date': []}
        self.factTable = None
        self.factTablePD = None
        

if __name__ == "__main__":
    x = StarSchema()
    x.loadExcel('data/global_superstore_2016.xlsx')
    x.readWorkSheet('Orders')
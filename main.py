# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

import random
import sys
import time
from os import path, sep

import numpy as np
import pandas as pd
import pyqtgraph as pg
from PyQt5 import QtCore, QtGui, QtWidgets

from DataManagement import DataManagement
from FileManagement import FileManagement
import json

from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QVBoxLayout
from PyQt5.QtGui import QIcon, QPixmap, QPainter

# ffffff
stylesheet = """
QMainWindow {
    background-color: #ff9900;
}
"""


class Ui_MainWindow(DataManagement, FileManagement):
    hasFile = False
    sheetState = None

    dimensions = {}
    measurements = {}

    afterFilterSelected = ()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1200, 812)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.dimensionLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dimensionLabel.sizePolicy().hasHeightForWidth())
        self.dimensionLabel.setSizePolicy(sizePolicy)
        self.dimensionLabel.setObjectName("dimensionLabel")
        self.gridLayout.addWidget(self.dimensionLabel, 0, 0, 1, 1)
        self.columnLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.columnLabel.sizePolicy().hasHeightForWidth())
        self.columnLabel.setSizePolicy(sizePolicy)
        self.columnLabel.setObjectName("columnLabel")
        self.gridLayout.addWidget(self.columnLabel, 0, 1, 1, 1)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")

        self.gridLayout.addWidget(self.tabWidget, 0, 2, 8, 1)
        self.dimensionListWidget = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dimensionListWidget.sizePolicy().hasHeightForWidth())

        self.dimensionListWidget.setSizePolicy(sizePolicy)
        self.dimensionListWidget.setDragEnabled(False)
        self.dimensionListWidget.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.dimensionListWidget.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.dimensionListWidget.setObjectName("dimensionListWidget")
        self.gridLayout.addWidget(self.dimensionListWidget, 1, 0, 1, 1)

        self.columnListWidget = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.columnListWidget.sizePolicy().hasHeightForWidth())

        self.columnListWidget.setSizePolicy(sizePolicy)
        self.columnListWidget.setDragEnabled(False)
        self.columnListWidget.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.columnListWidget.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.columnListWidget.setObjectName("columnListWidget")
        self.gridLayout.addWidget(self.columnListWidget, 1, 1, 1, 1)
        self.columnListWidget.currentItemChanged.connect(self.displayColumnFilter)
        self.columnListWidget.itemClicked.connect(self.displayColumnFilter)
        self.columnListWidget.itemChanged.connect(self.displayColumnFilter)

        self.measurementLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.measurementLabel.sizePolicy().hasHeightForWidth())
        self.measurementLabel.setSizePolicy(sizePolicy)
        self.measurementLabel.setObjectName("measurementLabel")
        self.gridLayout.addWidget(self.measurementLabel, 2, 0, 1, 1)
        self.rowLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.rowLabel.sizePolicy().hasHeightForWidth())
        self.rowLabel.setSizePolicy(sizePolicy)
        self.rowLabel.setObjectName("rowLabel")
        self.gridLayout.addWidget(self.rowLabel, 2, 1, 1, 1)
        self.measurementListWidget = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.measurementListWidget.sizePolicy().hasHeightForWidth())
        self.measurementListWidget.setSizePolicy(sizePolicy)
        self.measurementListWidget.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.measurementListWidget.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.measurementListWidget.setObjectName("measurementListWidget")
        self.gridLayout.addWidget(self.measurementListWidget, 3, 0, 1, 1)

        self.rowListWidget = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.rowListWidget.sizePolicy().hasHeightForWidth())

        self.rowListWidget.setSizePolicy(sizePolicy)
        self.rowListWidget.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.rowListWidget.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.rowListWidget.setObjectName("rowListWidget")
        self.gridLayout.addWidget(self.rowListWidget, 3, 1, 1, 1)
        self.rowListWidget.currentItemChanged.connect(self.displayRowFilter)
        self.rowListWidget.itemClicked.connect(self.displayRowFilter)
        self.rowListWidget.itemChanged.connect(self.displayRowFilter)

        self.filterLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.filterLabel.sizePolicy().hasHeightForWidth())
        self.filterLabel.setSizePolicy(sizePolicy)
        self.filterLabel.setObjectName("filterLabel")
        self.gridLayout.addWidget(self.filterLabel, 4, 0, 1, 1)
        self.visualizationLabel = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.visualizationLabel.sizePolicy().hasHeightForWidth())
        self.visualizationLabel.setSizePolicy(sizePolicy)
        self.visualizationLabel.setObjectName("visualizationLabel")
        self.gridLayout.addWidget(self.visualizationLabel, 4, 1, 1, 1)

        self.filterListWidget = QtWidgets.QListWidget(self.centralwidget)
        self.filterListWidget.setEnabled(True)
        self.filterListWidget.itemActivated.connect(self.toggle)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.filterListWidget.sizePolicy().hasHeightForWidth())
        self.filterListWidget.setSizePolicy(sizePolicy)
        self.filterListWidget.setObjectName("filterListWidget")
        self.gridLayout.addWidget(self.filterListWidget, 5, 0, 3, 1)

        self.barChartButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.barChartButton.sizePolicy().hasHeightForWidth())
        self.barChartButton.setSizePolicy(sizePolicy)
        self.barChartButton.setObjectName("barChartButton")
        self.gridLayout.addWidget(self.barChartButton, 5, 1, 1, 1)
        self.barChartButton.clicked.connect(lambda state: self.plot('barChart'))

        self.lineChartButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineChartButton.sizePolicy().hasHeightForWidth())
        self.lineChartButton.setSizePolicy(sizePolicy)
        self.lineChartButton.setObjectName("lineChartButton")
        self.lineChartButton.clicked.connect(lambda state: self.plot('lineChart'))

        self.gridLayout.addWidget(self.lineChartButton, 6, 1, 1, 1)
        self.pieChartButton = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pieChartButton.sizePolicy().hasHeightForWidth())
        self.pieChartButton.setSizePolicy(sizePolicy)
        self.pieChartButton.setObjectName("pieChartButton")
        self.gridLayout.addWidget(self.pieChartButton, 7, 1, 1, 1)
        self.pieChartButton.clicked.connect(lambda state: self.plot('pieChart'))

        MainWindow.setCentralWidget(self.centralwidget)

        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1200, 22))
        self.menubar.setObjectName("menubar")

        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")

        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        """Open xlsx file"""
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.actionOpen.triggered.connect(lambda state: self.openFileNameDialog('open')) #conncect menu open to openFileNameDialog function

        self.actionLoad = QtWidgets.QAction(MainWindow)
        self.actionLoad.setObjectName("actionLoad")
        self.actionLoad.triggered.connect(lambda state: self.openFileNameDialog('load')) #conncect menu load to openFileNameDialog function

        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        self.actionSave.triggered.connect(self.saveFileDialog)

        self.menuFile.addAction(self.actionOpen)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionLoad)
        self.menuFile.addAction(self.actionSave)

        self.menubar.addAction(self.menuFile.menuAction())
        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def retranslateUi(self, MainWindow):

        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Business Intelligence"))

        self.dimensionLabel.setText(_translate("MainWindow", "Dimensions"))
        self.columnLabel.setText(_translate("MainWindow", "Column"))
        self.measurementLabel.setText(_translate("MainWindow", "Measurements"))
        self.rowLabel.setText(_translate("MainWindow", "Row"))
        self.filterLabel.setText(_translate("MainWindow", "Filter"))
        self.visualizationLabel.setText(_translate("MainWindow", "Visualization"))
        self.barChartButton.setText(_translate("MainWindow", "Bar Chart"))
        self.lineChartButton.setText(_translate("MainWindow", "Line Chart"))
        self.pieChartButton.setText(_translate("MainWindow", "Pie Chart"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
        self.actionOpen.setShortcut(_translate("MainWindow", "Ctrl+O"))
        self.actionLoad.setText(_translate("MainWindow", "Load"))
        self.actionLoad.setShortcut(_translate("MainWindow", "Ctrl+L"))
        self.actionSave.setText(_translate("MainWindow", "Save"))
        self.actionSave.setShortcut(_translate("MainWindow", "Ctrl+S"))

        __sortingEnabled = self.filterListWidget.isSortingEnabled()
        self.filterListWidget.setSortingEnabled(__sortingEnabled)

    def openFileNameDialog(self, state):
        '''Open file function'''
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName()
        if fileName:
            '''Initial values'''
            sheetState = None
            dimensions = {}
            measurements = {}

            afterFilterSelected = ()
            self._resetState()

            typeFile = fileName.split('/')[-1].split('.')[1]
            workbookName = fileName.split('/')[-1].split('.')[0]
            isLoad = False
            """Open file"""
            #will clear menu when open file
            if (self.hasFile):
                self.menuWorkBook.clear()
                self.hasFile = False

            self.resetStarschema()

            self.setPath(fileName)

            if state == 'open': #check open state
                if typeFile != 'xlsx': 
                    return

                c = []
                with open('caches/_caches.json', 'r') as caches: #read md5 caches file
                    for row in caches:
                        c.append(row[:-1])

                if self.getMD5() in c:
                    """read data from caches"""
                    self.loadFile('{}.json'.format(self.getMD5()), state) #check current excel md5 in cache file
                    isLoad = True
                else:
                    """Load Workbook"""
                    self.loadSheet(fileName) #open first time or unsaved file 


            else:
                """Load file"""
                if typeFile != 'bi': #open .bi file
                    return

                self.loadFile(fileName, state)
                isLoad = True


        self.hasFile = True
        """Create menu workbook which can select the sheet to visualize and read work sheet"""
        _translate = QtCore.QCoreApplication.translate

        self.menuWorkBook = QtWidgets.QMenu(self.menubar)
        self.menuWorkBook.setObjectName("menuWorkBook")
        self.menuWorkBook.setTitle(_translate("MainWindow", workbookName))
        if isLoad:
            self.sheet = QtWidgets.QAction(MainWindow)
            self.sheet.setObjectName(self.currentSheet)
            self.sheet.setText(_translate("MainWindow", self.currentSheet))
            self.menuWorkBook.addAction(self.sheet)
            self.menubar.addAction(self.menuWorkBook.menuAction())

            self.querySheet(self.currentSheet, isLoad)
        else:
            for sheet in self.sheetNames:
                self.sheet = QtWidgets.QAction(MainWindow)
                self.sheet.setObjectName(sheet)
                self.sheet.setText(_translate("MainWindow", sheet))
                self.sheet.triggered.connect(lambda state, x=sheet: self.querySheet(x, False))
                self.menuWorkBook.addAction(self.sheet)
            self.menubar.addAction(self.menuWorkBook.menuAction())

            """Read first sheet by default"""
            firstSheet = list(self.sheetNames)[0]
            self.querySheet(firstSheet, False)


    def saveFileDialog(self):
        """save current data which visualize"""
        fileName = QtWidgets.QFileDialog.getSaveFileName()

        if fileName[0] != '':
            """save in caches"""

            ft = self.factTablePD.to_dict()
            for i in self.category['dimensionsDate']:
                ft.pop(i+' Timestamp')

            res = {
                'fileName': self.fileName,
                'currentSheet': self.currentSheet,
                'category': self.category,
                'factTable': ft,
                'dimensions': self._dimensions
            }

            c = []
            with open('caches/_caches.json', 'r') as caches:
                for row in caches:
                    c.append(row)

            if self.getMD5() not in c: #check md5 is not in cache file
                with open('caches/_caches.json', 'a') as caches:
                    caches.write('{}\n'.format(self.md5))

                with open('caches/{}.json'.format(self.md5), 'w') as md5:
                    json.dump(res, md5)

            with open('{}.bi'.format(fileName[0]), 'w') as md5: #save .bi file
                json.dump(res, md5)


    def querySheet(self, sheetName, state):
        """Read sheet and query data to gui"""

        if self.sheetState == sheetName: #not query same sheet
            return
        self.sheetState = sheetName
        self._resetState()
        if not state: # state == open
            self.readSheet(sheetName)
        self._getState(sheetName)

    def plot(self, chart):
        '''Plot Bar Chart, Line Chart or Pie Chart Function'''
        self.tabWidget.clear() #clear tab widget
        self.keepFilterOnChange()
        dimensions = [self.columnListWidget.item(i).text() for i in range(self.columnListWidget.count())] #get select dimensions from columnListWidget
        new_dims = []
        for dim in dimensions :
            if 'Date' in dim : #convert dimension date key to timestamp key and convert to {<DATE KEY> : <YEAR/MONTH/DAT/QUARTER>} form
                try :
                    count = 0
                    for i in self.dimensions[dim] :
                        if self.dimensions[dim][i] == True :
                            new_dims.append({dim + ' Timestamp': i})
                            count += 1
                            break
                    if count == 0 :
                        new_dims.append(dim + ' Timestamp')
                except :
                    new_dims.append(dim + ' Timestamp')
                continue
            new_dims.append(dim)
        dimensions = new_dims

        measures = [self.rowListWidget.item(i).text() for i in range(self.rowListWidget.count())] #get select mesures from rowListWidget
        
        func = 'sum'
        
        grouped = self.group(dimensions, measures, func, self._filterDF({**self.dimensions, **self.measurements})) #group and filter pandas DateFrame

        if chart == 'pieChart':
            '''Plot Pie Chart condition'''
            try:
                pieChart = grouped.plot.pie(subplots=True, figsize=(10, 10), autopct='%1.1f%%', legend=False, fontsize = 12) #plot with pandas plot
                fig = pieChart[0].get_figure()
                fig.savefig('plot.png') #save part to .png file

                self.chart = QtWidgets.QWidget()
                self.chart.setObjectName("chart")
                self.chartLayout = QtWidgets.QVBoxLayout()
                label = Label(self.chart)
                pixmap = QtGui.QPixmap("plot.png") #load chart .png file to show in GUI
                label.setPixmap(pixmap)
                self.chart.resize(pixmap.width(), pixmap.height())
                self.chartLayout.addWidget(label)
                self.chart.setLayout(self.chartLayout)
                self.tabWidget.addTab(self.chart, "Chart")
            except:
                pass

        else:
            '''Prepare to plot Bar chart or Line chart'''
            self.chart = QtWidgets.QWidget()
            self.chart.setObjectName("chart")
            self.chartLayout = QtWidgets.QVBoxLayout()
            self.plotWidget = pg.PlotWidget(name='Plot')
            self.plotWidget.setBackground('w')
            self.chartLayout.addWidget(self.plotWidget)
            self.chart.setLayout(self.chartLayout)

            self.tabWidget.addTab(self.chart, "Chart")
            self.table = QtWidgets.QWidget()
            self.table.setObjectName("table")

            self.plotWidget.clear()
            try:
                self.legend.scene().removeItem(self.legend)
            except Exception as e:
                pass

            self.legend = self.plotWidget.addLegend()

            dimensionsAxis = list(enumerate(list(map(str, grouped.index.values)))) #prepare dimention values to plot Bar or Line chart
            measurementAxis = grouped.loc[:, measures] #prepare measure values to plot Bar or Line chart
            colors = self._randomNColor(len(self.getCurrentSheetHeaders())) #random color for graph

            yMin = 0
            yMax = 0
            Xmin = grouped.shape[0]

            if Xmin > 20:
                Xmin = 20

            for eachMeasurement in measurementAxis:
                color = random.choice(colors)
                eachX = grouped.shape[0]
                eachY = [float(x) for x in list(grouped[eachMeasurement])]
                maxY = np.max(eachY)
                minY = np.min(eachY)
                if maxY > yMax:
                    yMax = maxY
                if minY < yMin:
                    yMin = minY
                if chart == 'barChart':
                    barChart = pg.BarGraphItem(x=np.arange(eachX), height=eachY, width=0.5, brush=color)
                    self.plotWidget.addItem(barChart)
                    self.plotWidget.plot(name=eachMeasurement, pen=color)
                elif chart == 'lineChart':
                    self.plotWidget.plot(x=np.arange(eachX), y=eachY, pen=color, symbol='x', symbolPen=color,
                                         name=eachMeasurement)

            self.plotWidget.setLimits(yMin=yMin, yMax=yMax)
            self.plotWidget.setXRange(0, Xmin)
            self.plotWidget.getAxis('bottom').setTicks([dimensionsAxis])

    def _randomNColor(self, numbers):
        '''Random n color.'''
        colors = []
        red = int(random.random() * 256)
        green = int(random.random() * 256)
        blue = int(random.random() * 256)
        step = 256 / numbers
        for i in range(numbers):
            red += step
            green += step
            blue += step
            red = int(red) % 256
            green = int(green) % 256
            blue = int(blue) % 256
            colors.append((red, green, blue))
        return colors

    def displayRowFilter(self):
        '''Show measure values for filter'''
        curSelected = self.rowListWidget.currentRow()
        
        if curSelected != -1:
            measurement = self.rowListWidget.item(curSelected).text()
            if self.afterFilterSelected and self.afterFilterSelected[0] == measurement:
                return

            self.keepFilterOnChange()

            dataMeasurement = self.factTablePD[measurement]
            _translate = QtCore.QCoreApplication.translate

            self.filterListWidget.clear()
            checkAll = QtWidgets.QListWidgetItem()
            checkAll.setText(_translate("MainWindow", "View all")) #View all
            ignoreAll = QtWidgets.QListWidgetItem()
            ignoreAll.setText(_translate("MainWindow", "Ignore all")) #Ignore all
            self.filterListWidget.addItem(checkAll)
            self.filterListWidget.addItem(ignoreAll)

            if measurement in self.measurements:
                for _ in self.measurements[measurement]:
                    #prepare measurement values loop
                    item = QtWidgets.QListWidgetItem()
                    item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
                    item.setText(_translate("MainWindow", str(_)))
                    if self.measurements[measurement][_] == True:
                        item.setCheckState(QtCore.Qt.Checked)
                    else:
                        item.setCheckState(QtCore.Qt.Unchecked)
                    self.filterListWidget.addItem(item)
            else:
                #if select measurement first time
                filterMeasurement = []
                for _ in dataMeasurement:
                    if _ not in filterMeasurement and str(_) != 'nan':
                        filterMeasurement.append(_)
                filterMeasurement.sort()
                for _ in filterMeasurement:
                    item = QtWidgets.QListWidgetItem()
                    item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
                    item.setCheckState(QtCore.Qt.Checked)
                    item.setText(_translate("MainWindow", str(_)))
                    self.filterListWidget.addItem(item)

            self.afterFilterSelected = ('measurement', measurement)

    def displayColumnFilter(self):
        '''Show dimension values for filter'''
        curSelected = self.columnListWidget.currentRow()
        
        if curSelected != -1:
            dimension = self.columnListWidget.item(curSelected).text()
            
            if self.afterFilterSelected and self.afterFilterSelected[0] == dimension:
                return
     
            self.keepFilterOnChange()
            _translate = QtCore.QCoreApplication.translate
            self.filterListWidget.clear()

            if dimension in self.category['dimensionsDate'] :
                #prepare date filter
                dim_date = ['Quarter', 'Year', 'Month', 'Day']
                blank = QtWidgets.QListWidgetItem()
                blank.setText(_translate("MainWindow", ""))
                blank2 = QtWidgets.QListWidgetItem()
                blank2.setText(_translate("MainWindow", ""))
                self.filterListWidget.addItem(blank)
                self.filterListWidget.addItem(blank2)
                for _ in dim_date:
                    item = QtWidgets.QListWidgetItem()
                    item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
                    item.setText(_translate("MainWindow", str(_)))
                    item.setCheckState(QtCore.Qt.Unchecked)
                    self.filterListWidget.addItem(item)
            else :
                #preare normal filter
                checkAll = QtWidgets.QListWidgetItem()
                checkAll.setText(_translate("MainWindow", "View all")) #view all
                ignoreAll = QtWidgets.QListWidgetItem()
                ignoreAll.setText(_translate("MainWindow", "Ignore all")) #ignore all
                self.filterListWidget.addItem(checkAll)
                self.filterListWidget.addItem(ignoreAll)

                if dimension in self.dimensions:
                    #prepare normal dimension values to filter
                    dim = list(self.dimensions[dimension])
                    dim.sort()
                    for _ in dim:
                        item = QtWidgets.QListWidgetItem()
                        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
                        item.setText(_translate("MainWindow", _))
                        if self.dimensions[dimension][_] == True:
                            item.setCheckState(QtCore.Qt.Checked)
                        else:
                            item.setCheckState(QtCore.Qt.Unchecked)
                        self.filterListWidget.addItem(item)
                else:
                    #if select dimension first time
                    dat = list(self._dimensions[dimension].values())
                    dataDimension = [str(i) for i in dat]
                    dataDimension.sort()
                    for _ in dataDimension:
                        item = QtWidgets.QListWidgetItem()
                        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
                        item.setCheckState(QtCore.Qt.Checked)
                        item.setText(_translate("MainWindow", str(_)))
                        self.filterListWidget.addItem(item)

            self.afterFilterSelected = ('dimension', dimension)

    def keepFilterOnChange(self):
        '''keep checkbox state'''
        if self.filterListWidget.count() > 0:
            if self.afterFilterSelected[0] == 'measurement':
                self.measurements[self.afterFilterSelected[1]] = {}
                for i in range(2, self.filterListWidget.count()):
                    item = self.filterListWidget.item(i)
                    self.measurements[self.afterFilterSelected[1]][item.text()] = True if item.checkState() == 2 else False
            elif self.afterFilterSelected[0] == 'dimension':
                self.dimensions[self.afterFilterSelected[1]] = {}
                for i in range(2, self.filterListWidget.count()):
                    item = self.filterListWidget.item(i)
                    self.dimensions[self.afterFilterSelected[1]][item.text()] = True if item.checkState() == 2 else False

        dim = [self.columnListWidget.item(i).text() for i in range(self.columnListWidget.count())]
        mes = [self.rowListWidget.item(i).text() for i in range(self.rowListWidget.count())]
        dimRemove = []
        mesRemove = []
        for i in self.dimensions:
            if i not in dim:
                dimRemove.append(i)

        for d in dimRemove:
            self.dimensions.pop(d)

        for i in self.measurements:
            if i not in mes:
                self.measurements.pop(i)
        for d in mesRemove:
            self.measurements.pop(d)


    def toggle(self):
        """Toggle checkbox in filter list widget"""
        curSelected = self.filterListWidget.currentItem()

        if curSelected.text() == 'View all':
            for i in range(2, self.filterListWidget.count()):
                item = self.filterListWidget.item(i)
                item.setCheckState(QtCore.Qt.Checked)
        elif curSelected.text() == 'Ignore all':
            for i in range(2, self.filterListWidget.count()):
                # if item.checkState() == 2:
                item = self.filterListWidget.item(i)
                item.setCheckState(QtCore.Qt.Unchecked)

    def _resetState(self):
        """Reset value"""
        self.dimensionListWidget.clear()
        self.measurementListWidget.clear()
        self.columnListWidget.clear()
        self.rowListWidget.clear()
        self.filterListWidget.clear()
        self.tabWidget.clear()
        try:
            self.plotWidget.clear()
        except :
            pass

    def _getState(self, sheetName):
        """Add dimension and measure of this sheet to gui"""
        self._addItem(self.category['dimensionsDate'], self.dimensionListWidget)
        self._addItem(self.category['dimensions'], self.dimensionListWidget)
        # self._addItem(['Date'], self.dimensionListWidget)
        self._addItem(self.category['measurements'], self.measurementListWidget)

    def _addItem(self, items, widget):
        """Add data gui"""
        for item in items:
            self.item = QtWidgets.QListWidgetItem(item)
            widget.addItem(self.item)

class Label(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        self.p = QPixmap()

    def setPixmap(self, p):
        self.p = p
        self.update()

    def paintEvent(self, event):
        if not self.p.isNull():
            painter = QPainter(self)
            painter.setRenderHint(QPainter.SmoothPixmapTransform)
            painter.drawPixmap(self.rect(), self.p)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(stylesheet)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
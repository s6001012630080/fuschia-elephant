import pandas as pd
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import random

class DataManagement :
    '''This class will manage the excel file by pandas'''
    def __init__(self) :
        self.excelSheet = {'current_sheet' : None, 'pandas_excel' : None, 'sheet_names' : None}

    def loadSheet(self, path) :
        '''Load sheet all from excel file as pandas object'''
        self.excelSheet['pandas_excel'] = pd.ExcelFile(str(path))
        self.loadSheetNames()

    def loadSheetNames(self) :
        '''Load sheet name from excel file'''
        self.excelSheet['sheet_names'] = self.excelSheet['pandas_excel'].sheet_names

    def readSheet(self, sheet_name) :
        '''Read data from sheet and classifly dimensions and measures'''
        self.excelSheet['current_sheet'] = str(sheet_name)
        self.excelSheet[self.excelSheet['current_sheet']] = {'dataFrame' : self.excelSheet['pandas_excel'].parse(str(sheet_name))}
        self.classifyDimMeas()
        self.createDimDateTable()
        self.currentSheetDf = self.excelSheet[self.excelSheet['current_sheet']]['dataFrame']

    def classifyDimMeas(self) :
        '''Classifly dimensions and measures'''
        colsType = {'dimensions' : [], 'measures' : []}
        df = self.excelSheet[self.excelSheet['current_sheet']]['dataFrame']
        for cols, dataTypes in zip(df.columns, df.dtypes) :
            if dataTypes == 'int64' or dataTypes == 'float64' :
                colsType['measures'].append(cols)
            else :
                colsType['dimensions'].append(cols)
        self.excelSheet[self.excelSheet['current_sheet']]['dimensions'] = colsType['dimensions']
        self.excelSheet[self.excelSheet['current_sheet']]['measures'] = colsType['measures']

    def createDimDateTable(self) :
        '''Create dimensions date table'''
        df = self.excelSheet[self.excelSheet['current_sheet']]['dataFrame']
        dimDateKeys = []
        dimDate = []
        for cols, dataTypes in zip(df.columns, df.dtypes) :
            if dataTypes == 'datetime64[ns]' :
                for vals in self.getCurrentSheetColsVal(cols) :
                    dateDict = dict()
                    if vals not in dimDateKeys :
                        dateDict['year'] = vals.year
                        dateDict['month'] = vals.month
                        dateDict['day'] = vals.day
                        dateDict['quarter'] = (int(vals.month) - 1) // 3
                        dimDate.append(dateDict)
                        dimDateKeys.append(vals)
        self.excelSheet[self.excelSheet['current_sheet']]['dimDate'] = dimDate

    def geCurrentSheetDimDate(self) :
        '''return dimensions date table in current sheet as list of dict'''
        return self.excelSheet[self.excelSheet['current_sheet']]['dimDate']

    def getCurrentSheetColsVal(self,header) :
        '''Return columns value in current sheet as list'''
        return list(self.excelSheet[self.excelSheet['current_sheet']]['dataFrame'][header])

    def getCurrentSheetColsDataType(self) :
        '''Return columns data type value in current sheet as dict with "dimensions" and "measures" keys'''
        current_sheet_data_types = {'dimensions' : self.excelSheet[self.excelSheet['current_sheet']]['dimensions'],
                                                   'measures' : self.excelSheet[self.excelSheet['current_sheet']]['measures']}
        return current_sheet_data_types

    def getSheetNames(self) :
        '''Return sheet names as list'''
        return self.excelSheet['sheet_names']

    def getCurrentSheetHeaders(self) :
        '''Return headers in current sheet'''
        return list(self.excelSheet[self.excelSheet['current_sheet']]['dataFrame'].columns)

    def group(self, dimensions, measures, func) :
        '''Return dict of groupby with measures is first level key and dimensions will groub in set is second level key'''
        #order.groupby(['Segment', 'City']).mean()[['Discount', 'Profit']]
        df = self.currentSheetDf
        dimensions_list = []
        for dim in dimensions :
            if type(dim) == dict :
                for dim_name in dim :
                    if dim[dim_name] == 'year' :
                        dimensions_list.append(df[dim_name].dt.year)
                    elif dim[dim_name] == 'month' :
                        dimensions_list.append(df[dim_name].dt.month)
                    elif dim[dim_name] == 'day' :
                        dimensions_list.append(df[dim_name].dt.day)
            else :
                dimensions_list.append(dim)
        current_sheet = self.excelSheet['pandas_excel'].parse(self.excelSheet['current_sheet'])
        if func == 'sum' :
            print(current_sheet.groupby(dimensions_list).sum()[measures])
            return current_sheet.groupby(dimensions_list).sum()[measures]
        elif func == 'mean' :
            return current_sheet.groupby(dimensions_list).mean()[measures]

    def randomNColor(self, numbers):
        '''Random n color.'''
        colors = []
        red = int(random.random() * 256)
        green = int(random.random() * 256)
        blue = int(random.random() * 256)
        step = 256 / numbers
        for i in range(numbers):
            red += step
            green += step
            blue += step
            red = int(red) % 256
            green = int(green) % 256
            blue = int(blue) % 256
            colors.append((red, green, blue))
        return colors

    def Plot(self, dimensions, measures, func, a_chart = 'barChart') :
        '''Plot Bar Chart or Line Chart'''
        #self.Plot([{'Order Date' : 'year'} ,'Segment'], ['Sales'], 'sum)
        #self.Plot([{'Order Date' : 'year'} ,'Segment'], ['Sales'], 'sum','barChart')
        #self.Plot([{'Order Date' : 'year'} ,'Segment'], ['Sales'], 'sum','lineChart')
        grouped = self.group(dimensions, measures, func)
        dimensionsAxis = list(enumerate(list(map(str, grouped.index.values))))
        measurementAxis = grouped.loc[:, measures]
        colors = self.randomNColor(len(self.getCurrentSheetHeaders()))

        win = pg.plot()
        win.setWindowTitle('pyqtgraph BarGraphItem')

        yMin = 0
        yMax = 0
        Xmin = grouped.shape[0]

        if Xmin > 20:
            Xmin = 20

        for eachMeasurement in measurementAxis:
            color = random.choice(colors)
            eachX = grouped.shape[0]
            eachY = [ float(x) for x in list(grouped[eachMeasurement]) ]
            maxY = np.max(eachY)
            minY = np.min(eachY)
            if maxY >  yMax:
                yMax = maxY
            if minY < yMin:
                yMin = minY
            if a_chart == 'barChart' :
                barChart = pg.BarGraphItem(x=np.arange(eachX), height=eachY, width=0.5, brush=color)
                win.addItem(barChart)
                win.plot(name=eachMeasurement, pen=color)
            elif a_chart == 'lineChart' :
                win.plot(x=np.arange(eachX), y=eachY, pen=color, symbol='x', symbolPen=color, name=eachMeasurement)

        win.setLimits(yMin= yMin, yMax=yMax)
        win.setXRange(0, Xmin)
        win.getAxis('bottom').setTicks([dimensionsAxis])

        if __name__ == '__main__':
            import sys
            if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
                QtGui.QApplication.instance().exec_()


if __name__ == '__main__':
    dm = DataManagement()
    print('LOADING EXCEL...')
    dm.loadSheet("data/global_superstore_2016.xlsx")
    print('LOAD EXCEL COMPLETE...')
    print('#####SHEET NAMES#####')
    print(dm.getSheetNames())
    print('READING "Orders" SHEET...')
    dm.readSheet('Orders')
    #print('#####HEADERS IN CURRENT SHEET#####')
    #print(dm.getCurrentSheetHeaders())
    #print('#####VALUES FORM SEGMENT COLUMN IN CURRENT SHEET#####')
    #print(dm.getCurrentSheetColsVal('Segment'))
    #print('#####DATA TYPE IN CURRENT SHEET#####')
    #print(dm.getCurrentSheetColsDataType())
    #print('#####DIMENSION DATE TABLE IN CURRENT SHEET#####')
    #print(dm.geCurrentSheetDimDate())
    #print('GROUPING DIMENSIONS AND MEASURES...')
    #print(dm.group(['Segment', 'Country', 'City'], ['Discount', 'Profit'], 'sum'))
    # print('PLOTING...')
    # dm.Plot([{'Order Date' : 'year'} ,'Segment'], ['Sales'], 'sum','lineChart')
    x = dm.excelSheet[dm.excelSheet['current_sheet']]['dimDate']
    print(x)

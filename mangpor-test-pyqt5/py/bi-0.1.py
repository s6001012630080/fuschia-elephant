# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'bi-0.1.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
# mangpor create

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1106, 628)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.SheetsTab = QtWidgets.QTabWidget(self.centralwidget)
        self.SheetsTab.setGeometry(QtCore.QRect(20, 10, 161, 181))
        self.SheetsTab.setObjectName("SheetsTab")
        self.SubSheetsTap = QtWidgets.QWidget()
        self.SubSheetsTap.setObjectName("SubSheetsTap")
        self.SheetList = QtWidgets.QListView(self.SubSheetsTap)
        self.SheetList.setGeometry(QtCore.QRect(0, 0, 151, 151))
        self.SheetList.setObjectName("SheetList")
        self.SheetsTab.addTab(self.SubSheetsTap, "")
        self.DimensionsTab = QtWidgets.QTabWidget(self.centralwidget)
        self.DimensionsTab.setGeometry(QtCore.QRect(20, 200, 161, 181))
        self.DimensionsTab.setObjectName("DimensionsTab")
        self.SubDimensionsTab = QtWidgets.QWidget()
        self.SubDimensionsTab.setObjectName("SubDimensionsTab")
        self.DimensionsList = QtWidgets.QListView(self.SubDimensionsTab)
        self.DimensionsList.setGeometry(QtCore.QRect(0, 0, 151, 151))
        self.DimensionsList.setObjectName("DimensionsList")
        self.DimensionsTab.addTab(self.SubDimensionsTab, "")
        self.MeasuresTab = QtWidgets.QTabWidget(self.centralwidget)
        self.MeasuresTab.setGeometry(QtCore.QRect(20, 390, 161, 181))
        self.MeasuresTab.setObjectName("MeasuresTab")
        self.SubMeasuresTab = QtWidgets.QWidget()
        self.SubMeasuresTab.setObjectName("SubMeasuresTab")
        self.MeasuresList = QtWidgets.QListView(self.SubMeasuresTab)
        self.MeasuresList.setGeometry(QtCore.QRect(0, 0, 151, 151))
        self.MeasuresList.setObjectName("MeasuresList")
        self.MeasuresTab.addTab(self.SubMeasuresTab, "")
        self.GraphAndTableTab = QtWidgets.QTabWidget(self.centralwidget)
        self.GraphAndTableTab.setGeometry(QtCore.QRect(200, 10, 621, 561))
        self.GraphAndTableTab.setObjectName("GraphAndTableTab")
        self.GraphTab = QtWidgets.QWidget()
        self.GraphTab.setObjectName("GraphTab")
        self.GraphAndTableTab.addTab(self.GraphTab, "")
        self.TableTab = QtWidgets.QWidget()
        self.TableTab.setObjectName("TableTab")
        self.GraphAndTableTab.addTab(self.TableTab, "")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(830, 10, 261, 181))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(830, 200, 261, 181))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(830, 390, 261, 181))
        self.pushButton_3.setObjectName("pushButton_3")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1106, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.SheetsTab.setCurrentIndex(0)
        self.DimensionsTab.setCurrentIndex(0)
        self.MeasuresTab.setCurrentIndex(0)
        self.GraphAndTableTab.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.SheetsTab.setTabText(self.SheetsTab.indexOf(self.SubSheetsTap), _translate("MainWindow", "Tab 2"))
        self.DimensionsTab.setTabText(self.DimensionsTab.indexOf(self.SubDimensionsTab), _translate("MainWindow", "Tab 2"))
        self.MeasuresTab.setTabText(self.MeasuresTab.indexOf(self.SubMeasuresTab), _translate("MainWindow", "Tab 2"))
        self.GraphAndTableTab.setTabText(self.GraphAndTableTab.indexOf(self.GraphTab), _translate("MainWindow", "Tab 1"))
        self.GraphAndTableTab.setTabText(self.GraphAndTableTab.indexOf(self.TableTab), _translate("MainWindow", "Tab 2"))
        self.pushButton.setText(_translate("MainWindow", "PushButton"))
        self.pushButton_2.setText(_translate("MainWindow", "PushButton"))
        self.pushButton_3.setText(_translate("MainWindow", "PushButton"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


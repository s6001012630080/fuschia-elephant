import pandas as pd
import numpy as np
from StarSchema import StarSchema


class DataManagement(StarSchema):
    '''This class will manage the excel file by pandas'''

    def loadSheet(self, path):
        '''Load sheet all from excel file as pandas object'''
        self.loadExcel(path)
        self.loadSheetNames()

    def loadSheetNames(self):
        '''Load sheet name from excel file'''
        self.sheetNames = self.workbook.sheet_names

    def readSheet(self, sheet_name):
        '''Read data from sheet and classifly dimensions and measures'''
        self.readWorkSheet(sheet_name)


    def _filterDF(self, filter_dict):
        '''Filter DateFrame function'''
        self.currentSheetFilterDf = self.factTablePD
        for i in filter_dict:
            isin_filter = []
            if 'Date' not in i :
                for j in filter_dict[i]:
                    if filter_dict[i][j] == False:
                        #get value that do not want for plot
                        try :
                            isin_filter.append(self._findDimNameFromID(i,j))
                        except :
                            isin_filter.append(j)
                self.currentSheetFilterDf = self.currentSheetFilterDf.loc[~self.currentSheetFilterDf[i].isin(isin_filter)] #pandas filter function
        return self.currentSheetFilterDf

    def _findDimNameFromID(self, col, ID):
        '''Find dimention value by ID'''
        for key in self._dimensions[col]:
            if self._dimensions[col][key] == ID:
                return key

    def getCurrentSheetHeaders(self):
        '''Return headers in current sheet'''
        return list(self.factTablePD.columns)

    def group(self, dimensions, measures, func, df):
        '''Return dict of groupby with measures is first level key and dimensions will groub in set is second level key'''
        u_df = df
        dimensions_list = []
        col_dict = {}
        for col in u_df :
            #convert DateFrame with ID form to DateFrame with value form loop
            col_dict[col] = []
            if col in self._dimensions :
                for row in u_df[col] :
                    col_dict[col].append(self._dimensions[col][str(row)])
            else :
                for row in u_df[col] :
                    col_dict[col].append(row)
        u_df = pd.DataFrame(col_dict)



        for dim in dimensions:
            #group DataFrame by datetime
            if type(dim) == dict:
                for dim_name in dim:
                    if dim[dim_name] == 'Quarter':
                        dimensions_list.append(u_df[dim_name].dt.quarter)
                    elif dim[dim_name] == 'Year':
                        dimensions_list.append(u_df[dim_name].dt.year)
                    elif dim[dim_name] == 'Month':
                        dimensions_list.append(u_df[dim_name].dt.month)
                    elif dim[dim_name] == 'Day':
                        dimensions_list.append(u_df[dim_name].dt.day)
            else:
                dimensions_list.append(dim)
        #group DataFrame condition
        if func == 'sum':
            return u_df.groupby(dimensions_list).sum()[measures]
        elif func == 'mean':
            return u_df.groupby(dimensions_list).mean()[measures]
